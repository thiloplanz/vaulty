Welcome to Vaulty 
===============================================================================


Vaulty is a command line tool to encrypt files using Hashicorp Vault.

It is written in Scala so that its functionality can also be used
as a library in Scala (or other JVM-based) applications.

### Installation

- have a JDK (Java 11 or newer) on your system
- [download the launcher script](https://bitbucket.org/thiloplanz/vaulty/downloads) and place it into your `$PATH`

Alternatively, you can also [run it via Docker](docker-image/).

### Encrypting and decrypting files

```
vaulty encrypt --key secret message.txt
```

This will read the file `message.txt` and encrypt it using the Vault transit secret called `secret`.
The resulting encrypted version is placed into a new file `message.txt.vaulty` next to the 
input file.

```
vaulty decrypt --key secret message.txt.vaulty
```

To get the data back, decrypt the file the Vaulty created with the same transit secret.

##### How it works

Vaulty uses Vault's transit encryption engine. This way, the encryption keys 
never have to leave Vault and access to them can be controlled (and audited) there.
For this to work, you need to provide Vaulty with an access token to encrypt or decrypt
using the specified secret (encryption and decryption could be granted separately,
so that for example an automated backup job only needs to be given permission to encrypt
without being able to decrypt).

If the input data is small (up to 1024 bytes by default), this will be just
a direct API call to Vault to encrypt that data itself.

For larger data, a hybrid approach is used: Vaulty generates a one-time AES-256 encryption key
and uses that to locally encrypt the input. That AES is then encrypted by Vault,
and the encrypted key is stored along with the encrypted payload in Vaulty's output file.


### Configuring Vault access

##### Preparing transit encryption

You need to enable the transit engine in Vault on the default path of `/v1/transit`,
create an encryption key there and prepare the necessary policies to access
it for encryption and decryption.

The name of the key is not important (and you can use any number of them), 
as the name is always specified in the Vaulty command that needs to use it.

##### Network access to Vault

Vaulty looks at the environment variable `VAULT_ADDR`.

The default of `http://127.0.0.1:8200/` can be used to connect to a Vault
in developer mode.

##### Access token

You need to provide Vaulty with its access token in the environment variable `VAULT_TOKEN`.

If `VAULT_ADDR` is not given, Vaulty tries to find a local Vault CLI login 
session token at `~/.vault-token`. This way, you can use the `vault login` 
command to authenticate first and then use Vaulty within that session.

There is no default setting for this, you will need to configure a token
even in developer mode.

The only situation where it can be omitted is if you are using the Vault
Agent in auto-auth mode. In this case, Vaulty can connect to Vault Agent
over a trusted connection without a token and let the agent take care of
authentication.

##### Command line instead of environment variables

Instead of environment variables, configuration can also be passed in using 
Java system properties. Note the `-J-D` prefix.

```
vaulty -J-Dvaulty.vault_addr=http://127.0.0.1:8200/ -J-Dvaulty.vault_token=ABCD encrypt --key secret file.txt
```

This mechanism can also be used to configure [a number of other
parameters](src/main/resources/reference.conf).


### Using remote files via S3

A very common use-case for Vaulty are encrypted backups,
and a very common requirement for backups is to store them
somewhere else. To help with this. Vaulty supports
reading from and writing to remote storage systems via the 
S3 protocol.

The access details and credentials for that are configured
within Vault.

```
vault encrypt --key secret --out s3://bucket:/path/x.vaulty localFile.txt 

and in vault at /v1/secret/vaulty/s3/bucket

AWS_ACCESS_KEY_ID = nnnn
AWS_SECRET_ACCESS_KEY = nnnnn
AWS_REGION = us-east-1       

# optional for AWS, but needed if you use someone else's service
S3_ENDPOINT = https://s3.hidrive.strato.com 

# if the bucket is actually called something else (not matching the name
# from the command line path), but can normally be left empty
S3_BUCKET_NAME = not-bucket  
```

Vaulty can also just copy files between local and remote locations
(without applying any encryption), so that you do not have to
get another tool for that:

```
vaulty copy s3://bucket:/a.txt s3://bucket:/b.txt
```


### Key derivation with context

The Vault transit engine supports key derivation as an additional
security measure. Here, the transit key is combined with a user-supplied
additional "context" to arrive at a different, context-specific 
encryption key.

Vault supports this by letting you pass in the context as a command line flag:

```
vaulty encrypt --key secret --context for-offsite-backup file.zip
```

Note that this context is not stored in the output and must
also be provided to the decryption step. So if you use this
context to inject another secret value, you have to keep track
of it out-of-band.


### Retrieving secrets

For convenience, Vaulty also includes its version of `vault kv get` to 
retrieve secrets from Vault (for example to check on the S3 configuration).

```
vaulty show mySecret
vaulty show --out x.json mySecret
```
