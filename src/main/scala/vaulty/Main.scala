// Copyright 2020-2021, Thilo Planz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vaulty

import java.io.{BufferedOutputStream, File, FileInputStream, FileNotFoundException, FileOutputStream, IOException, InputStream, OutputStream, PipedInputStream, PipedOutputStream}
import java.net.http.HttpRequest
import java.nio.file.{Files, NoSuchFileException}

import CommandLineArguments._
import com.typesafe.config.ConfigFactory
import org.rogach.scallop.exceptions.ScallopException
import org.rogach.scallop.{ScallopConf, Subcommand}
import play.api.libs.json.Json
import sttp.client3.httpclient.zio.{HttpClientZioBackend, SttpClient}
import zio.blocking.Blocking
import zio.clock.Clock
import zio.{Chunk, ExitCode, Managed, Task, ZIO}
import zio.console._
import zio.stream.{Sink, Stream, ZSink, ZStream}

// for scallop
import scala.language.reflectiveCalls

/**
 * The Vaulty command line app.
 *
 * @author Thilo Planz
 */
object VaultyApp extends zio.App {

  private val config = ConfigFactory.load().getConfig("vaulty")
  val vaultAddr = config.getString("vault_addr")
  val transitPath = vaultAddr + config.getString("transit_path")
  val kvPath = vaultAddr + config.getString("kv_path")
  val token =
    if (config.hasPath("vault_token")) Some(config.getString("vault_token"))
    else {
      val localTokenFile = new File(config.getString("vault_token_helper"))
      if (localTokenFile.exists()){
        Some(Files.readString(localTokenFile.toPath).trim)
      } else None
    }

  private def derivedDecryptedName(inputPath: String) =
    new File(inputPath).getName.replaceAll("\\.vaulty$", "")

  private def derivedEncryptedName(inputPath: String) =
    new File(inputPath).getName + ".vaulty"

  private def openOutputFile(outputPath: String, outputName: String): ZSink[SttpClient with Clock with Blocking, IOException, Byte, Byte, Unit] =
    outputPath match {
      case "-" => Sink.fromOutputStream(System.out).map(_ => ())
      case s"s3://$bucket:$path" => S3.writeToS3(bucket, path)
      case s"s3:$x" =>
        ZSink.fail(new IOException("Invalid s3 output specification.\nThis should look like s3://bucketName:/path/to/file"))
      case path =>
        Sink.fromOutputStreamManaged(openOutputFile(new File(outputPath), outputName)).map(_ => ())
    }


  private def openOutputFile(file: File, fallbackName: String): Managed[IOException, OutputStream] =
    if (file.isDirectory) openOutputFile(new File(file, fallbackName), fallbackName)
    else if (file.exists()) Managed.fail(new IOException(s"Output file ${file.getAbsoluteFile} already exists. Vaulty will not overwrite it."))
    else Managed.fromAutoCloseable(Task{new BufferedOutputStream(new FileOutputStream(file))}.refineToOrDie)
      .onExit(exit => Task(file.delete()).ignore.when(!exit.succeeded))

  private def openInputFile(name: String) =
    name match {
      case "-" => ZStream.fromInputStream(System.in)
      case s"s3://$bucket:$path" => S3.readFromS3(bucket, path)
      case s"s3:$x" => ZStream.fail(new IOException("Invalid s3 input specification.\nThis should look like s3://bucketName:/path/to/file"))
      case path => ZStream.fromFile(new File(path).toPath).refineToOrDie[IOException]
    }

  private def encrypt() = {
    val in = Encrypt.in()
    val out = openOutputFile(Encrypt.out.toOption.getOrElse(in + ".vaulty"), derivedEncryptedName(in))
    val smallFileLimit = config.getInt("small_file_limit")
      openInputFile(Encrypt.in()).chunkN(Math.max(32 * 1024, smallFileLimit + 1))
        .mapChunks(Chunk.single).peel(ZSink.head[Chunk[Byte]])
        .use {
        case (Some(head), rest) if head.length <= smallFileLimit =>
          for {
            more <- rest.runCollect // this should be empty here
            encrypted <- VaultAPI.encryptWithTransit(transitPath, token, Encrypt.key(), None, head ++ more.flatten)
            _ <- Stream.fromIterable(encrypted.getBytes()).run(out)
          } yield ()
        case (Some(head), rest) =>
          VaultAPI.encryptWithAES(transitPath, token, Encrypt.key(), Encrypt.context.toOption, Stream.fromChunk(head) ++ rest.flattenChunks, out)
        case (None, _) =>
          Stream.fromEffect(
            VaultAPI.encryptWithTransit(transitPath, token, Encrypt.key(), None, Chunk.fromArray(Array.emptyByteArray))
              .map(e => Chunk.fromArray(e.getBytes()))
          ).flattenChunks.run(out)
      }
    }

  private def decrypt() = {
    val (in, out) = Decrypt.in() match {
      case "-" =>
        openInputFile("-") -> openOutputFile(Decrypt.out.toOption.getOrElse("-"), "stdin.vaulty")
      case file if file.endsWith(".vaulty") =>
        val outName = derivedDecryptedName(file)
        openInputFile(file) -> openOutputFile(Decrypt.out.toOption.getOrElse(file.dropRight(7)), outName)
      case file =>
        val outName = derivedDecryptedName(file)
        openInputFile(file+".vaulty") -> openOutputFile(Decrypt.out.toOption.getOrElse(file), outName)
    }

    in.transduce(Crypto.decryptVaultyStream(transitPath, token, Decrypt.key(), Decrypt.context.toOption)).run(out)
  }

  private def copy() = openInputFile(Copy.in()).run(openOutputFile(Copy.out(), Copy.out()))


  private def readSecret() = for {
    secret <- VaultAPI.getSecret(kvPath, token, Show.name())
    _ <- Crypto.byteStream(
        Json.prettyPrint(Json.toJson(secret)), "\n"
      ).run(openOutputFile(Show.out.toOption.getOrElse("-"), "stdout.vaulty"))
  } yield ()


  def run(args: List[String]) = {
    // check for Java 11
    ZIO.succeed(
      try{
        Right(HttpRequest.newBuilder())
      } catch { case e: Throwable => Left(e) }
    ).flatMap {
      case Left(e) =>
        putStrLnErr(s"Bootstrap failure, make sure you are using Java 11 or newer\n${e}")
          .ignore
          .as(ExitCode.failure)
      case Right(_) =>
        (for {
          parsed <- Task(new CommandLineArguments(args))
          _ <- parsed.subcommand match {
            case Some(Encrypt) => encrypt()
            case Some(Decrypt) => decrypt()
            case Some(Show) => readSecret()
            case Some(Copy) => copy()
            case _ =>
              putStrLnErr("missing command") *> putStrLnErr(parsed.builder.bann.getOrElse(""))
          }
        } yield ExitCode.success)
          .catchSome {
            case e: ScallopException =>
              putStrLnErr(e.toString).as(ExitCode.failure)
            case e: NoSuchFileException =>
              putStrLnErr(e.toString).as(ExitCode.failure)
            case io: IOException =>
              putStrLnErr(io.getMessage).as(ExitCode.failure)
          }.provideCustomLayer(HttpClientZioBackend.layer()).orDie
    }
  }
}


private[vaulty] class CommandLineArguments(args: Seq[String]) extends ScallopConf(args){
  banner(
    """Usage:
      |  vaulty encrypt --key key file [outputFile]
      |  vaulty decrypt --key key file.vaulty [outputFile]
      |""".stripMargin)
  addSubcommand(Encrypt)
  addSubcommand(Decrypt)
  addSubcommand(Show)
  addSubcommand(Copy)

  verify()
}
private[vaulty] trait CommonOptions {
  _: ScallopConf =>

  val key = opt[String]("key", required = true, descr = "The name of the Vault key to use")

  val context = opt[String]("context", descr = "User-supplied context for Vault Transit Key Derivation.")
}


private[vaulty] object CommandLineArguments {
  val Encrypt = new Subcommand("encrypt") with CommonOptions {
    val in = trailArg[String]("inputFile", "The file to be encrypted, set to '-' to read data from console input")
    val out = trailArg[String]("outputFile", required = false, descr = "Specify where the output should be written to (if not given, will be derived from the name of the input)")
  }
  val Decrypt = new Subcommand("decrypt") with CommonOptions {
    val in = trailArg[String]("inputFile", "The file to be decrypted, set to '-' to read data from console input.")
    val out = trailArg[String]("outputFile", required = false, descr = "Specify where the output should be written to (if not given, will be derived from the name of the input)")
  }
  val Show = new Subcommand("show") {
    val out = opt[String]("out", descr = "Specify where the secret should be written to (if not given, prints it to the console)")
    val name = trailArg[String]("name", "The name of the secret to retrieve")
  }
  val Copy = new Subcommand("copy") {
    val in = trailArg[String]("inputFile", "The file to be transferred, set to '-' to read data from console input")
    val out = trailArg[String]("outputFile", "The target location for the file, set to '-' to write to the console.")
  }

}
