// Copyright 2020-2021, Thilo Planz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


scalaVersion := "2.13.6"
val zioVersion = "1.0.11"
val sttpVersion = "3.3.14"

libraryDependencies += "dev.zio" %% "zio" % zioVersion
libraryDependencies += "dev.zio" %% "zio-streams" % zioVersion
libraryDependencies += "dev.zio" %% "zio-s3" % "0.3.7"

libraryDependencies += "org.rogach" %% "scallop" % "4.0.4"
libraryDependencies += "com.softwaremill.sttp.client3" %% "httpclient-backend-zio" % sttpVersion
libraryDependencies += "com.softwaremill.sttp.client3" %% "play-json" % sttpVersion

libraryDependencies += "com.typesafe" % "config" % "1.4.1"
libraryDependencies += "org.slf4j" % "slf4j-nop" % "1.7.30"

libraryDependencies += "dev.zio" %% "zio-test-sbt" % zioVersion % Test

testFrameworks := Seq(new TestFramework("zio.test.sbt.ZTestFramework"))

scalacOptions += "-feature"

val myVersion = "0.0.3-SNAPSHOT"
version := myVersion
organization := "org.bitbucket.thiloplanz"
name := "vaulty"

lazy val coursierBootstrap = taskKey[Unit]("Creates the bootstrap binary")
coursierBootstrap := {
  import scala.sys.process._
  val s: TaskStreams = streams.value
  s"./coursier bootstrap org.bitbucket.thiloplanz:vaulty_2.13:${myVersion} -f -o target/vaulty-${myVersion}" !
}
