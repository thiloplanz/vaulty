// Copyright 2020-2021, Thilo Planz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vaulty

import java.io.{File, FileInputStream, FileOutputStream, IOException, OutputStream}
import java.nio.charset.StandardCharsets
import java.security.GeneralSecurityException

import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}
import javax.crypto._
import sttp.client3.httpclient.zio.SttpClient
import zio.blocking.Blocking
import zio.nio.file.Files
import zio.stream.{ZSink, ZStream, ZTransducer}
import zio._

/**
 * Crypto helpers
 *
 * @author Thilo Planz
 */
object Crypto {

  private val AES = "AES/CBC/PKCS5Padding"

  // we are using a random one-time session key that is never re-used, so should be okay to have an IV of 0
  val ZERO_IV = new IvParameterSpec(Array.fill[Byte](16)(0))

  def generateAESKey(size: Int = 256) = Task {
    val keyGen = KeyGenerator.getInstance("AES")
    keyGen.init(size)
    keyGen.generateKey()
  }

  def getAESCipherForEncryption(key: SecretKey, iv: IvParameterSpec): ZManaged[Any, GeneralSecurityException, Cipher] = Task {
    val encipher = Cipher.getInstance(AES)
    encipher.init(Cipher.ENCRYPT_MODE, key, iv)
    encipher
  }.toManaged_.refineToOrDie

  def getAESCipherForDecryption(key: SecretKey, iv: IvParameterSpec): ZManaged[Any, GeneralSecurityException, Cipher] = Task {
    val decipher = Cipher.getInstance(AES)
    decipher.init(Cipher.DECRYPT_MODE, key, iv)
    decipher
  }.toManaged_.refineToOrDie

  def encryptVaultyStream[R](algorithmTag: String, cipherFactory: ZManaged[R, Throwable, Cipher], encryptedKey: String): ZTransducer[R, GeneralSecurityException, Byte, Byte] =
    encryptStream(cipherFactory.orDie) >>> ZTransducer.prepend(Chunk.fromArray(("[" + algorithmTag + "][" + encryptedKey + "]").getBytes(StandardCharsets.UTF_8)))

  def encryptStream[R](cipherFactory: ZManaged[R, GeneralSecurityException, Cipher]): ZTransducer[R, GeneralSecurityException, Byte, Byte] = ZTransducer {
    cipherFactory.map[Option[Chunk[Byte]] => ZIO[Any, GeneralSecurityException, Chunk[Byte]]](cipherPush).refineToOrDie
  }


  private def cipherPush[R](cipher: Cipher) : Option[Chunk[Byte]] => ZIO[R, GeneralSecurityException, Chunk[Byte]] =  {
    case None =>
      ZIO(Chunk.fromArray(cipher.doFinal())).catchSome{case e: BadPaddingException => UIO(Chunk.empty) }.refineToOrDie
    case Some(bytes: Chunk[Byte]) =>
      ZIO(Chunk.fromArray(cipher.update(bytes.toArray))).refineToOrDie
  }

  private val vaultPlusAES = "[vault+aes]".getBytes(StandardCharsets.UTF_8)
  private val vaultTransit = "vault:".getBytes(StandardCharsets.UTF_8)

  def decryptVaultyStream[R <: SttpClient](transitPath: String, vaultToken: Option[String], keyName: String, context: Option[String]): ZTransducer[R, Exception, Byte, Byte] = {
    ZTransducer.branchAfter(vaultPlusAES.length) { header =>
      if (header.startsWith(vaultPlusAES)) {
        ZTransducer.branchAfter(1024) { keySegment: Chunk[Byte] =>
          val encryptedKey = keySegment.drop(1).takeWhile(_ != ']')
          val rest = keySegment.drop(encryptedKey.length + 2)
          ZTransducer.prepend(rest) >>> ZTransducer {
            for {
              sessionKey <- VaultAPI.decryptWithTransit(transitPath, vaultToken, keyName, context, new String(encryptedKey.toArray)).toManaged_.orDie
              key = new SecretKeySpec(sessionKey.toArray, "AES")
              cipher <- getAESCipherForDecryption(key, ZERO_IV).orDie
            } yield cipherPush[R](cipher)
          }
        }
      } else if (header.startsWith(vaultTransit)){
        ZTransducer.prepend(header) >>> ZTransducer.collectAllN[Byte](1024*1024) >>> ZTransducer.fromPush[SttpClient, IOException, Chunk[Byte], Byte] {
          case None => ZIO.succeed(Chunk.empty)
          case Some(chunk) => VaultAPI.decryptWithTransit(transitPath, vaultToken, keyName, context, new String(chunk.flatten.toArray))
        }
      } else ZTransducer.fail(new IllegalArgumentException("Unsupported encryption format"))
    }
  }

  def byteStream(strings: String*) =
    ZStream.fromChunks(strings.map(s => Chunk.fromArray(s.getBytes(StandardCharsets.UTF_8))): _*)

  /**
   * Drains the stream into a buffer, then returns a new Stream to read it back again,
   * as well as the length of the stream.
   *
   * The buffer will start in-memory, but can spill to a temporary file
   * (which will be encrypted to protect it -- and of course it is still deleted afterwards)
   */
  def determineStreamLength[R <: Blocking](in: ZStream[R, IOException, Byte]): ZManaged[R, IOException, (Long, ZStream[Blocking, IOException, Byte])] = {
    val buffer = 1024 * 1024
    for {
      start <- in.chunkN(4096).peel(ZSink.take[Byte](buffer))
      (head, tail) = start
      result <- if (head.length < buffer) {
        ZManaged.succeed(head.length.longValue -> ZStream.fromChunk(head))
      } else for {
        // write the rest into a temporary and encrypted file, count the bytes along the way
        key <- ZManaged.fromEffect(generateAESKey()).orDie
        file <- ZManaged.make(Files.createTempFile(prefix = Some("vaulty-"), fileAttributes = Seq.empty))(Files.delete(_).ignore)
        jFile = file.toFile
        count <- tail.run(ZSink.count.zipPar(
          encryptStream(getAESCipherForEncryption(key, ZERO_IV))  >>>
          ZSink.fromFile(jFile.toPath))).toManaged_.refineToOrDie
      } yield (head.length + count._1, ZStream.fromChunk(head) ++
        ZStream.fromInputStream(new FileInputStream(jFile))
        .transduce(encryptStream(getAESCipherForDecryption(key, ZERO_IV).orDie)).refineToOrDie
      )
    } yield result
  }
}
