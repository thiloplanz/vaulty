// Copyright 2020-2021, Thilo Planz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vaulty

import zio.Chunk
import zio.stream.ZStream
import zio.test.{DefaultRunnableSpec, assert, suite, testM}
import zio.test.Assertion._



object CryptoSpec extends DefaultRunnableSpec {
  val large = 2_000_000

  def spec = suite("CryptoSpec")(
   testM("roundtrip simple AES encryption") {
      val secret = "A secret piece of information. Gonna encrypt it now."
      for {
        key <- Crypto.generateAESKey()
        encipher = Crypto.getAESCipherForEncryption(key, Crypto.ZERO_IV).orDie
        decipher = Crypto.getAESCipherForDecryption(key, Crypto.ZERO_IV).orDie
        output      <- ZStream.fromChunk(Chunk.fromArray(secret.getBytes)).chunkN(4096)
          .transduce(Crypto.encryptStream(encipher))
          .transduce(Crypto.encryptStream(decipher))
          .runCollect.map(chunk => new String(chunk.toArray))
      } yield assert(output)(equalTo(secret))
    },

    testM("roundtrip simple AES encryption for large data") {
      for {
        key <- Crypto.generateAESKey()
        encipher = Crypto.getAESCipherForEncryption(key, Crypto.ZERO_IV).orDie
        decipher = Crypto.getAESCipherForDecryption(key, Crypto.ZERO_IV).orDie
        output      <- ZStream.repeat(42.byteValue()).take(large).chunkN(4096)
          .transduce(Crypto.encryptStream(encipher))
          .transduce(Crypto.encryptStream(decipher))
          .runCollect
      } yield assert(output)(equalTo(Chunk.fill(large)(42.byteValue)))
    },


    testM("count stream length for small data"){
      for {
        result <- Crypto.determineStreamLength(ZStream.repeat(42.byteValue).take(100)).use {
          case (len, stream) =>
            stream.runCollect.map(len -> _)
        }
      } yield assert(result._1)(equalTo(100L)) &&
        assert(result._1)(equalTo(result._2.length.longValue())) &&
        assert(result._2)(equalTo(Chunk.fill(100)(42.byteValue)))
    },

    testM("count stream length for large data"){
      for {
        result <- Crypto.determineStreamLength(ZStream.repeat(42.byteValue).take(large)).use {
          case (len, stream) =>
            stream.runCollect.map(len -> _)
        }
      } yield assert(result._1)(equalTo(large.longValue())) &&
        assert(result._1)(equalTo(result._2.length.longValue())) &&
        assert(result._2)(equalTo(Chunk.fill(large)(42.byteValue)))
    }
  )
}