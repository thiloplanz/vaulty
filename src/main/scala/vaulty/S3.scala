// Copyright 2020-2021, Thilo Planz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vaulty

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials

import java.io.IOException
import java.net.URI
import java.time.Duration
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.model.NoSuchKeyException
import sttp.client3.httpclient.zio.SttpClient
import vaulty.VaultyApp.{kvPath, token}
import zio._
import zio.blocking.Blocking
import zio.clock.Clock
import zio.s3._
import zio.stream.ZSink.Push
import zio.stream.{Stream, ZSink, ZStream}

/**
 * Code to interact with S3-compatible Object Storage
 *
 * @author Thilo Planz
 */

object S3 {

  private def getS3Info(bucket: String) = {
    val secretName = s"vaulty/s3/$bucket"
    for {
      s3conf <- VaultAPI.getSecret(kvPath, token, secretName)
      s3key <- ZIO.fromOption(s3conf.get("AWS_ACCESS_KEY_ID")).orElseFail(new IOException(s"Could not find AWS_ACCESS_KEY_ID for S3 access at ${kvPath}/data/${secretName}"))
      s3secret <- ZIO.fromOption(s3conf.get("AWS_SECRET_ACCESS_KEY")).orElseFail(new IOException(s"Could not find AWS_SECRET_ACCESS_KEY for S3 access at ${kvPath}/data/${secretName}"))
      s3region <- ZIO.fromOption(s3conf.get("AWS_REGION"))
        .orElseFail(new IOException(s"Could not find AWS_REGION for S3 access at ${kvPath}/data/${secretName}"))
        .map(Region.of)
      bucketName = s3conf.getOrElse("S3_BUCKET_NAME", bucket)
      s3endpoint = s3conf.get("S3_ENDPOINT")
    } yield bucketName -> live(s3region, AwsBasicCredentials.create(s3key, s3secret), s3endpoint.map(new URI(_)))
  }

  def readFromS3(bucket: String, path: String): ZStream[SttpClient, IOException, Byte] =
    for {
      info <- ZStream.fromEffect(getS3Info(bucket))
      (bucketName, layer) = info
      bytes <- getObject(bucketName, path)
        .provideLayer(layer)
        .catchAll { s3Exception =>
          ZStream.fail(new IOException(s"Downloading from S3 bucket '$bucketName' failed\n${s3Exception}\nCheck that the configured access key can read '${path}' from bucket '${bucketName}'"))
        }
    } yield bytes

  def writeToS3(bucket: String, path: String): ZSink[SttpClient with Clock with Blocking, IOException, Byte, Byte, Unit] = {
    ZSink.managed(
      for {
        info <- getS3Info(bucket).toManaged_
        (bucketName, layer) = info
        _ <- getObjectMetadata(bucketName, path)
          .provideLayer(layer)
          .foldM(s3Exception =>
            if (s3Exception.isInstanceOf[NoSuchKeyException]) ZIO.unit
            else ZIO.fail(new IOException(s"Connecting to S3 bucket '$bucketName' failed\n${s3Exception}\nCheck that the configured access key can access '${path}' in bucket '${bucketName}'"))
            , _ =>
              ZIO.fail(new IOException(s"Output file '$path' already exists in bucket '$bucketName'. Vaulty will not overwrite it."))
          ).toManaged_
        queue <- Queue.bounded[Chunk[Byte]](8).toManaged_
        stream = Stream.fromChunkQueue(queue)
        uploader <- Crypto.determineStreamLength(stream).use { withLength =>
          putObject[Blocking](bucketName, path, withLength._1, withLength._2)
            .provideSomeLayer[Blocking with Clock](layer)
            .catchAll { s3Exception =>
              ZIO.fail(new IOException(s"Uploading to S3 bucket '$bucketName' failed\n${s3Exception}\nCheck that the configured access key can write to '${path}' in bucket '${bucketName}'"))
            }
        }.forkDaemon.toManaged_
      } yield (queue, uploader)
    ) {
      case (queue, fiber) =>
        ZSink.fromPush {
          x: Option[Chunk[Byte]] =>
            x match {
              case Some(chunk) =>
                queue.offer(chunk) *> Push.more
              case None =>
                for {
                  left <- queue.size
                  _ <- (ZIO.sleep(Duration.ofMillis(20)) *> queue.size).repeatUntil(_ < 1).when(left > 0)
                  _ <- queue.shutdown
                  finish <- fiber.join.either
                  result <- finish.fold(
                    error => Push.fail(error, Chunk.empty),
                    _ => Push.emit((), Chunk.empty)
                  )
                } yield result

            }
        }

    }
  }

}
