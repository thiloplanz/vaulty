Welcome to Vaulty 
===============================================================================

Vaulty is a command line tool to encrypt files using Hashicorp Vault.

This document describes how to use it via a Docker image.
For other installation options and general information about Vaulty,
see https://bitbucket.org/thiloplanz/vaulty/

### What's in this Docker image?

The Vaulty docker image also includes the official Vault command-line tool.
You can access that by changing the entrypoint
to `vault` or by running `/usr/sbin/vault` from a shell into the container.

And since Vaulty is written in Scala, you also get an OpenJDK.

```
# run vaulty
docker run --rm thiloplanz/vaulty show mySecret
# run vault
docker run --rm --entrypoint vault thiloplanz/vaulty status
# interactive shell
docker run --rm -it --entrypoint sh thiloplanz/vaulty
# jdk tools
docker run --rm -it --entrypoint jshell thiloplanz/vaulty
```

### Network access to Vault and access token handling

You need to tell the tool how it can reach Vault. This is done via the environment variable VAULT_ADDR.

```
docker run --rm -e VAULT_ADDR=http://host.docker.internal:8200/ -e VAULT_TOKEN=xxxxx thiloplanz/vaulty show mySecret
```

A safer alternative to using an environment variable for the vault token is to put it into a file
and make that available as `/root/.vault-token`. This way you can also use the session from the `vault login` 
command-line tool.

```
docker run --rm -e VAULT_ADDR=http://host.docker.internal:8200/ --mount src="$HOME/.vault-token",target=/root/.vault-token,readonly,type=bind thiloplanz/vaulty show mySecret
```

You could also create and use the token in an interactive session.

```
docker run --rm -it --entrypoint sh -e VAULT_ADDR=http://host.docker.internal:8200/ thiloplanz/vaulty

/workspace # vault login -method=userpass username=me
Password (will be hidden): 

/workspace # vaulty show mySecret

```


### Mounting volumes into the container

In order for Vaulty running as a Docker container
to work on your files, you have to attach the volumes
that contain them.

The recommended mount path is `/workspace`, which
is also the default working directory when the
tool is run (but any other paths should also work).

```
docker run --rm --mount src="$(pwd)",target=/workspace,type=bind thiloplanz/vaulty encrypt --key secret file.txt
```

