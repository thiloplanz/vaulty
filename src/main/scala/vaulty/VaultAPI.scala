// Copyright 2020-2021, Thilo Planz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vaulty

import java.io.IOException
import java.nio.charset.StandardCharsets
import java.util.Base64

import play.api.libs.json.Json
import sttp.client3.httpclient.zio._
import sttp.model.Uri
import zio.{Chunk, ZIO}
import zio.stream.{ZSink, ZStream}
import StandardCharsets.UTF_8
import java.net.ConnectException

import sttp.client3._
import sttp.client3.playJson._
import zio.blocking.Blocking


/**
 * Code to interact with Hashicorp Vault
 *
 * @author Thilo Planz
 */
object VaultAPI {

  private implicit val jsonData = Json.format[VaultAPIResponseData]
  private implicit val jsonResp = Json.format[VaultAPIResponse]

  private def callVault(request: Request[Either[String, String], Any], vaultToken: Option[String]) =
    send(
      vaultToken.fold(request)(token =>
        request.header("X-Vault-Token", token)
      )
        .header("X-Vault-Request", "true")
        .response(asJson[VaultAPIResponse]))
      .map(_.body)
      .absolve
      .refineOrDie {
        case e: SttpClientException.ConnectException =>
          new ConnectException(
            s"""Failed to connect to Vault.
               |${e.getMessage}
               |Make sure you have configured VAULT_ADDR properly.""".stripMargin)
        case e: HttpError[_] if e.statusCode.code == 400 && e.body.toString.contains("missing client token") =>
          val extra = vaultToken.fold("No token is currently set. This will only work if you are using Vault Agent in Auto-Auth mode.")(
            t => s" The token used just now was '${t.take(4)}****'.")
          new IOException(
            s"""Vault access denied.
               |${e.getMessage}
               |Make sure you have configured VAULT_TOKEN properly. ${extra}""".stripMargin
          )
        case e: HttpError[_] if e.statusCode.code == 403 && e.body.toString.contains("permission denied") =>
          val extra = vaultToken.fold("No token is currently set. This will only work if you are using Vault Agent in Auto-Auth mode.")(
            t => s" The token used just now was '${t.take(4)}****'.")
          new IOException(
            s"""Vault access denied.
               |${e.getMessage}
               |Make sure you have configured VAULT_TOKEN properly and that the token has access to the required policies. ${extra}""".stripMargin
          )
        case e: HttpError[_] if e.statusCode.code == 404 =>
          new IOException(
            s"""Vault resource not found.
               |${e.getMessage}
               |Make sure the path '${request.uri.path.mkString("/")}' matches the configured secret engines and keys""".stripMargin
          )
        case e: HttpError[_] if e.statusCode.code == 400 && e.body.toString.contains("encryption key not found") =>
          new IOException(
            s"""Encryption key not found.
               |${e.getMessage}
               |Make sure the path '${request.uri.path.mkString("/")}' matches the configured secret engines and keys""".stripMargin
          )
        case e: HttpError[_] if e.statusCode.code == 400 && e.body.toString.contains("unable to decrypt") =>
          new IOException(
            s"""Decryption failed.
               |${e.getMessage}
               |Did you use the right key and/or derivation context?""".stripMargin
          )
        case e: HttpError[_] if e.statusCode.code == 503 && e.body.toString.contains("Vault is sealed") =>
          new IOException(
            s"""Vault is sealed.
               |The Vault at '${request.uri.host}' is sealed.""".stripMargin
          )
      }

  def encryptWithTransit(transitPath: String, vaultToken: Option[String], keyName: String, context: Option[String], data: Chunk[Byte]): ZIO[SttpClient, IOException, String] = for {
    vaultResponse <- callVault(
      basicRequest.body(Json.obj(
        "plaintext" -> Base64.getEncoder.encodeToString(data.toArray),
        "context" -> context.map(c => Base64.getEncoder.encodeToString(c.getBytes(UTF_8)))
      )).post(Uri.unsafeParse(s"$transitPath/encrypt/$keyName")), vaultToken
    )
    cipherText <- ZIO.succeed(vaultResponse)
      .map(_.data.flatMap(_.ciphertext))
      .someOrFail(new IOException(s"Unexpected response from Vault did not contain the ciphertext.\n${vaultResponse}"))
  } yield cipherText

  def decryptWithTransit(transitPath: String, vaultToken: Option[String], keyName: String, context: Option[String], ciphertext: String): ZIO[SttpClient, IOException, Chunk[Byte]] = for {
    vaultResponse <- callVault(
      basicRequest.body(Json.obj(
        "ciphertext" -> ciphertext,
        "context" -> context.map(c => Base64.getEncoder.encodeToString(c.getBytes(UTF_8)))
      )).post(Uri.unsafeParse(s"$transitPath/decrypt/$keyName")), vaultToken
    )
    plainText <- ZIO.succeed(vaultResponse)
      .map(_.data.flatMap(_.plaintext))
      .someOrFail(new IOException(s"Unexpected response from Vault did not contain the decrypted plaintext.\n${vaultResponse}"))
  } yield Chunk.fromArray(Base64.getDecoder.decode(plainText))


  def encryptWithAES[R <: Blocking](transitPath: String, vaultToken: Option[String], keyName: String, context: Option[String], data: ZStream[R, IOException, Byte], out: ZSink[R, IOException, Byte, Byte, Unit]): ZIO[SttpClient with R, IOException, Unit] = for {
    sessionKey <- Crypto.generateAESKey().refineToOrDie
    vaultResponse <- callVault(
      basicRequest.body(Json.obj(
        "plaintext" -> Base64.getEncoder.encodeToString(sessionKey.getEncoded),
        "context" -> context.map(c => Base64.getEncoder.encodeToString(c.getBytes(UTF_8)))
      )).post(Uri.unsafeParse(s"$transitPath/encrypt/$keyName")), vaultToken
    )
    encryptedKey <- ZIO.succeed(vaultResponse)
      .map(_.data.flatMap(_.ciphertext))
      .someOrFail(new IOException(s"Unexpected response from Vault did not contain the ciphertext.\n${vaultResponse}"))
    _ <- data.chunkN(4096).transduce(Crypto.encryptVaultyStream("vault+aes", Crypto.getAESCipherForEncryption(sessionKey, Crypto.ZERO_IV), encryptedKey)).run(out).refineToOrDie[IOException]
  } yield ()

  def getSecret(kvPath: String, vaultToken: Option[String], secretName: String) =
    callVault(
      basicRequest.get(Uri.unsafeParse(s"$kvPath/data/$secretName")), vaultToken
    ).map(_.data.flatMap(_.data).getOrElse(Map.empty))
}

case class VaultAPIResponse(
                             data: Option[VaultAPIResponseData],
                             errors: Option[Seq[String]]
                           )

case class VaultAPIResponseData(
                                 // "ciphertext": "vault:v1:XjsPWPjqPrBi1N2Ms2s1QM798YyFWnO4TR4lsFA="
                                 ciphertext: Option[String],
                                 plaintext: Option[String],
                                 data: Option[Map[String, String]]
                               )

